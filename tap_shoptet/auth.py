"""hubspot Authentication."""

import json
import backoff
import datetime
from typing import Any, Dict, Optional

import requests
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.streams import Stream as RESTStreamBase


class MaxiumTokensReached(Exception):
    pass


class ShoptetAuthenticator(APIAuthenticatorBase):
    """API Authenticator for Shoptet."""

    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap

    @property
    def auth_headers(self) -> dict:
        """Return a dictionary of auth headers to be applied.

        These will be merged with any `http_headers` specified in the stream.

        Returns:
            HTTP headers for authentication.
        """
        if not self.is_token_valid():
            self.update_access_token()
        result = super().auth_headers
        result["Shoptet-Access-Token"] = str(self._tap._config.get("access_token"))
        return result

    @property
    def auth_endpoint(self) -> str:
        """Get the authorization endpoint.

        Returns:
            The API authorization endpoint if it is set.

        Raises:
            ValueError: If the endpoint is not set.
        """
        if not self._auth_endpoint:
            raise ValueError("Authorization endpoint not set.")
        return self._auth_endpoint

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the hubspot API."""
        return {}
    
    def get_current_timestamp(self):
        #return timezone according to utc timezone
        return round(datetime.datetime.now(datetime.timezone.utc).timestamp())

    def is_token_valid(self) -> bool:
        access_token = self._tap._config.get("access_token")
        now = self.get_current_timestamp()
        expires_in = self._tap._config.get("expires_in")

        return not bool(
            (not access_token) or (not expires_in) or ((expires_in - now) < 60)
        )

    @property
    def oauth_request_payload(self) -> dict:
        """Get request body.

        Returns:
            A plain (OAuth) or encrypted (JWT) request body.
        """
        return self.oauth_request_body

    # Authentication and refresh
    @backoff.on_exception(backoff.constant, MaxiumTokensReached, interval=60, jitter=None, max_time=1800)
    def update_access_token(self) -> None:
        """Update `access_token` along with: `last_refreshed` and `expires_in`.

        Raises:
            RuntimeError: When OAuth login fails.
        """
        api_key = self._tap._config["api_key"]
        request_time = self.get_current_timestamp()

        headers = {
            "Authorization": f"Bearer {api_key}",
            "Content-Type": "application/vnd.shoptet.v1.0",
        }
        token_response = requests.get(self.auth_endpoint, headers=headers)
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            response = token_response.json()
            if response.get("error")=="maximum_tokens_reached":
                raise MaxiumTokensReached
            raise RuntimeError(
                f"Failed OAuth login, response was '{response}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        expires_in = request_time + token_json["expires_in"]

        self._tap._config["access_token"] = token_json["access_token"]
        self._tap._config["expires_in"] = expires_in
        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)
