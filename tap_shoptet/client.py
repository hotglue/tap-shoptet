"""REST client handling, including shoptetStream base class."""
import os
import gzip
import io
import json
import requests
from datetime import datetime
from time import sleep
from typing import Any, Callable, Dict, Iterable, List, Optional

import backoff
import requests
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

from tap_shoptet.auth import ShoptetAuthenticator


class shoptetStream(RESTStream):
    """shoptet stream class."""

    url_base = "https://api.myshoptet.com/api/"

    records_jsonpath = "$[*]"
    replication_filter = None
    additional_params = None

    @backoff.on_exception(
        backoff.expo,
        (RetriableAPIError,
        requests.exceptions.ReadTimeout,
        requests.exceptions.ConnectionError,
        requests.exceptions.RequestException,
        ConnectionError,),
        max_tries=10,
        factor=3,
    )
    def get_webhook(self,headers: dict):
        # Get the webhooks that are registered already
        resp = requests.get("https://api.myshoptet.com/api/webhooks", headers=headers)
        self.validate_response(resp)
        data = resp.json()
        return data

    def check_webhook(self):
        """
        Registers the job:finished webhook if not already pressent
        """
        headers = self.http_headers
        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
        webhooks = []
        # Get the webhooks that are registered already
        data = self.get_webhook(headers=headers)
        if data and isinstance(data, dict) and "data" in data:
            # Get the value associated with "data" key
            data = data["data"]
            # Check if "webhooks" is present in the nested dictionary
            if isinstance(data, dict) and "webhooks" in data:
                webhooks = data["webhooks"]
                # Continue processing with the "webhooks" data

        webhook = [w for w in webhooks if w.get("event") == "job:finished"]

        # the webhook for async endpoints is already registered
        if webhook:
            self.logger.info("Found existing job:finished webhook, skipping registration")
            return

        # We have to register it
        env_id = os.environ.get("ENV_ID", "")
        flow_id = os.environ.get("FLOW", "")
        resp = requests.post(
            "https://api.myshoptet.com/api/webhooks",
            headers=headers,
            data=json.dumps({
                "data": [
                    {
                        "event": "job:finished",
                        "url": f"https://client-api.hotglue.xyz/webhook/shoptet/{env_id}/{flow_id}",
                    }
                ]
            }),
        )

        self.logger.info("Registered job:finished webhook for async requests. response=[{resp.text}]")

    @property
    def authenticator(self) -> ShoptetAuthenticator:
        """Return a new authenticator object."""
        base_url = f"https://{self.config['shop']}.myshoptet.com"
        url = f"{base_url}/action/ApiOAuthServer/getAccessToken"
        return ShoptetAuthenticator(self, self._tap.config_file, url)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers["Content-Type"] = "application/vnd.shoptet.v1.0"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        paginator = response.json().get("data").get("paginator")
        if not paginator:
            return None
        total_page = paginator.get("pageCount")
        page = paginator.get("page")
        if page < total_page:
            next_page_token = page + 1
        else:
            next_page_token = None
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.additional_params:
            params.update(self.additional_params)
        if self.replication_key:
            filter = self.replication_filter or "creationTimeFrom"
            self.start_date = self.get_starting_timestamp(context)
            params[filter] = self.start_date.strftime("%Y-%m-%dT%H:%M:%S+0000")

        # Check webhook before making request
        self.check_webhook()

        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        if self.replication_key:
            for record in extract_jsonpath(
                self.records_jsonpath, input=response.json()
            ):
                record_mod_date = datetime.strptime(
                    record[self.replication_key], "%Y-%m-%dT%H:%M:%S%z"
                )
                if record_mod_date > self.start_date:
                    yield record
        else:
            yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        if context:
            row.update(context)
        return row

    def request_decorator(self, func: Callable) -> Callable:
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
                requests.exceptions.RequestException,
                ConnectionError,
            ),
            max_tries=5,
            factor=2,
        )(func)
        return decorator
    
    def validate_response(self, response: requests.Response) -> None:
        
        if response.status_code == 429:
            self.logger.warn(response.text)
            raise RetriableAPIError(f"Too Many Requests for path: {self.path}")
        
        elif 400 <= response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            #Log the error
            self.logger.warn(response.text)
            raise FatalAPIError(msg)

        elif 500 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            #Log the error
            self.logger.warn(response.text)
            raise RetriableAPIError(msg)


class ShoptetBulk(shoptetStream):
    bulk_time_limit = 43000

    def get_job_status(self, job_id):
        url = f"{self.url_base}system/jobs/{job_id}"
        headers = self.http_headers
        headers.update(self.authenticator.auth_headers)
        response = requests.get(url, headers=headers)
        result = response.json()
        if result.get("errors"):
            self.logger.warn(f"Problem with the bulk job: {result['errors']}")
            raise FatalAPIError("Error with shoptet bulk job.")
        elif result["data"]["job"]["completionTime"] is None:
            return False
        result_url = result["data"]["job"]["resultUrl"]
        return result_url

    def process_gzip(self, job_id):
        running_time = 0
        while True:
            if running_time > self.bulk_time_limit:
                raise FatalAPIError("Bulk job timed out.")
            response = self.request_decorator(self.get_job_status)(job_id)
            if response is not False:
                break
            else:
                self.logger.info(
                    f"Waiting for bulk job to finish for {self.name}, sleeping for 1 minute."
                )
                sleep_time = 60
                sleep(sleep_time)
                running_time += sleep_time
        if response:
            r = requests.get(response, headers=self.http_headers)
            with gzip.open(io.BytesIO(r.content), "rb") as f:
                for line in f.readlines():
                    yield json.loads(line)

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        job_id = response.json()["data"]["jobId"]

        if self.replication_key:
            for record in self.process_gzip(job_id):
                record_mod_date = datetime.strptime(
                    record[self.replication_key], "%Y-%m-%dT%H:%M:%S%z"
                )
                if record_mod_date > self.start_date:
                    yield record
        else:
            yield from self.process_gzip(job_id)
