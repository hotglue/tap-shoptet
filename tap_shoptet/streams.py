"""Stream type classes for tap-shoptet."""

import copy
from datetime import datetime, timedelta
from typing import Any, Dict, Iterable, Optional

import requests
from singer_sdk import typing as th
from singer_sdk.helpers.jsonpath import extract_jsonpath

from tap_shoptet.client import ShoptetBulk, shoptetStream


class ProductsStream(ShoptetBulk):
    """Define Products Stream"""

    name = "products"
    path = "/products/snapshot"
    primary_keys = ["guid"]
    additional_params = {
        "include": "variantParameters,flags,descriptiveParameters,setItems"
    }
    # replication_key = "changeTime"
    records_jsonpath = "$.data.products[*]"

    schema = th.PropertiesList(
        th.Property("guid", th.StringType),
        th.Property("adult", th.BooleanType),
        th.Property("atypicalBilling", th.BooleanType),
        th.Property("atypicalShipping", th.BooleanType),
        th.Property("visibility", th.StringType),
        th.Property("creationTime", th.DateTimeType),
        th.Property("changeTime", th.DateTimeType),
        th.Property("name", th.StringType),
        th.Property("type", th.StringType),
        th.Property("shortDescription", th.StringType),
        th.Property("description", th.StringType),
        th.Property("metaDescription", th.StringType),
        th.Property("metaTitle", th.StringType),
        th.Property("xmlFeedName", th.StringType),
        th.Property("additionalName", th.StringType),
        th.Property("internalNote", th.StringType),
        th.Property("allowIPlatba", th.BooleanType),
        th.Property("allowOnlinePayments", th.BooleanType),
        th.Property("sizeIdName", th.StringType),
        th.Property("voteCount", th.IntegerType),
        th.Property("voteAverageScore", th.StringType),
        th.Property(
            "defaultCategory",
            th.ObjectType(
                th.Property("guid", th.StringType), th.Property("name", th.StringType)
            ),
        ),
        th.Property("url", th.StringType),
        th.Property(
            "flags",
            th.ArrayType(
                th.ObjectType(
                    th.Property("code", th.StringType),
                    th.Property("dateFrom", th.DateTimeType),
                    th.Property("dateTo", th.DateTimeType),
                    th.Property("title", th.StringType),
                )
            ),
        ),
        th.Property("url", th.StringType),
        th.Property(
            "supplier",
            th.ObjectType(
                th.Property("guid", th.StringType), th.Property("name", th.StringType)
            ),
        ),
        th.Property(
            "brand",
            th.ObjectType(
                th.Property("code", th.StringType), th.Property("name", th.StringType)
            ),
        ),
        th.Property(
            "variants",
            th.ArrayType(
                th.ObjectType(
                    th.Property("code", th.StringType),
                    th.Property("ean", th.StringType),
                    th.Property("stock", th.StringType),
                    th.Property("unit", th.StringType),
                    th.Property("weight", th.StringType),
                    th.Property("visible", th.BooleanType),
                    th.Property("minStockSupply", th.StringType),
                    th.Property("negativeStockAllowed", th.StringType),
                    th.Property("amountDecimalPlaces", th.IntegerType),
                    th.Property("price", th.StringType),
                    th.Property("includingVat", th.BooleanType),
                    th.Property("vatRate", th.StringType),
                    th.Property("currencyCode", th.StringType),
                    th.Property(
                        "actionPrice",
                        th.ObjectType(
                            th.Property("fromDate", th.DateTimeType),
                            th.Property("price", th.StringType),
                            th.Property("toDate", th.DateTimeType),
                        ),
                    ),
                    th.Property("commonPrice", th.StringType),
                    th.Property("manufacturerCode", th.StringType),
                    th.Property("pluCode", th.StringType),
                    th.Property("isbn", th.StringType),
                    th.Property("serialNo", th.StringType),
                    th.Property("mpn", th.StringType),
                    th.Property("heurekaCPC", th.StringType),
                    th.Property(
                        "availability",
                        th.ObjectType(
                            th.Property("id", th.IntegerType),
                            th.Property("name", th.StringType),
                        ),
                    ),
                    th.Property(
                        "availabilityWhenSoldOut",
                        th.ObjectType(
                            th.Property("id", th.IntegerType),
                            th.Property("name", th.StringType),
                        ),
                    ),
                    th.Property("name", th.StringType),
                ),
            ),
        ),
        th.Property(
            "descriptiveParameters",
            th.ArrayType(
                th.ObjectType(
                    th.Property("name", th.StringType),
                    th.Property("value", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("priority", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "setItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("amount", th.StringType),
                    th.Property("code", th.StringType),
                    th.Property("guid", th.StringType),
                )
            ),
        ),
    ).to_dict()


class StocksListStream(shoptetStream):
    """Define Stocks List Stream"""

    name = "stocks_list"
    path = "/stocks"
    primary_keys = ["id"]
    records_jsonpath = "$.data.stocks[*]"
    state_partitioning_keys = ["stocks_list"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("title", th.StringType),
        th.Property("isDeliveryPoint", th.BooleanType),
        th.Property("deliveryPointTitle", th.StringType),
        th.Property("deliveryPointAddress", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"stock_id": record["id"]}


class StocksSuppliesStream(shoptetStream):
    """Define custom stream."""

    name = "stocks"
    path = "/stocks/{stock_id}/supplies"
    primary_keys = ["productGuid"]
    records_jsonpath = "$.data.supplies[*]"
    parent_stream_type = StocksListStream

    schema = th.PropertiesList(
        th.Property("stock_id", th.IntegerType),
        th.Property("productGuid", th.StringType),
        th.Property("code", th.StringType),
        th.Property("amount", th.StringType),
        th.Property("claim", th.StringType),
        th.Property("location", th.StringType),
    ).to_dict()


class PriceListStream(shoptetStream):
    """Define Stocks List Stream"""

    name = "prices_list"
    path = "/pricelists"
    primary_keys = ["id"]
    records_jsonpath = "$.data.pricelists[*]"
    state_partitioning_keys = ["prices_list"]

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"pricelist_id": record["id"]}


class PricesStream(shoptetStream):
    """Define custom stream."""

    name = "prices"
    path = "/pricelists/{pricelist_id}"
    primary_keys = ["code"]
    records_jsonpath = "$.data.pricelist[*]"
    parent_stream_type = PriceListStream

    schema = th.PropertiesList(
        th.Property("pricelist_id", th.IntegerType),
        th.Property("code", th.StringType),
        th.Property("currencyCode", th.StringType),
        th.Property("includingVat", th.BooleanType),
        th.Property("vatRate", th.StringType),
        th.Property(
            "price",
            th.ObjectType(
                th.Property("price", th.StringType),
                th.Property("commonPrice", th.StringType),
                th.Property("buyPrice", th.StringType),
                th.Property("priceRatio", th.StringType),
                th.Property(
                    "actionPrice",
                    th.ObjectType(
                        th.Property("fromDate", th.DateTimeType),
                        th.Property("price", th.StringType),
                        th.Property("toDate", th.DateTimeType),
                    ),
                ),
            ),
        ),
        th.Property(
            "orderableAmount",
            th.ObjectType(
                th.Property("minimumAmount", th.StringType),
                th.Property("maximumAmount", th.StringType),
            ),
        ),
        th.Property(
            "sales",
            th.ObjectType(
                th.Property("minPriceRatio", th.StringType),
                th.Property("freeShipping", th.BooleanType),
                th.Property("freeBilling", th.BooleanType),
                th.Property("loyaltyDiscount", th.BooleanType),
                th.Property("volumeDiscount", th.BooleanType),
                th.Property("quantityDiscount", th.BooleanType),
                th.Property("discountCoupon", th.BooleanType),
            ),
        ),
    ).to_dict()


class CurrenciesStream(shoptetStream):
    """Define Stocks List Stream"""

    name = "currencies"
    path = "/eshop"
    primary_keys = ["code"]
    records_jsonpath = "$.data.currencies[*]"

    schema = th.PropertiesList(
        th.Property("code", th.StringType),
        th.Property("title", th.StringType),
        th.Property("isDefault", th.BooleanType),
        th.Property("isDefaultAdmin", th.BooleanType),
        th.Property("isVisible", th.BooleanType),
        th.Property("exchangeRate", th.StringType),
        th.Property("priority", th.IntegerType),
        th.Property(
            "display",
            th.ObjectType(
                th.Property("text", th.StringType),
                th.Property("location", th.StringType),
                th.Property("decimalsSeparator", th.StringType),
                th.Property("thousandsSeparator", th.StringType),
            ),
        ),
        th.Property("priceDecimalPlaces", th.IntegerType),
        th.Property("rounding", th.StringType),
        th.Property("minimalOrderValue", th.StringType),
        th.Property(
            "bankAccount",
            th.ObjectType(
                th.Property("accountNumber", th.StringType),
                th.Property("iban", th.StringType),
                th.Property("bic", th.StringType),
            ),
        ),
    ).to_dict()


class OrdersStream(ShoptetBulk):
    """Define Order stream."""

    name = "orders"
    path = "/orders/snapshot"
    records_jsonpath = "$.data.order"
    primary_keys = ["code"]
    additional_params = {"include": "stockLocation,images"}
    replication_key = "changeTime"
    replication_filter = "changeTimeFrom"

    schema = th.PropertiesList(
        th.Property("code", th.StringType),
        th.Property("externalCode", th.StringType),
        th.Property("creationTime", th.DateTimeType),
        th.Property("changeTime", th.DateTimeType),
        th.Property("email", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("birthDate", th.StringType),
        th.Property("clientCode", th.StringType),
        th.Property("companyId", th.StringType),
        th.Property("vatId", th.StringType),
        th.Property("taxId", th.StringType),
        th.Property("vatPayer", th.BooleanType),
        th.Property("customerGuid", th.StringType),
        th.Property("addressesEqual", th.BooleanType),
        th.Property("cashDeskOrder", th.BooleanType),
        th.Property("stockId", th.IntegerType),
        th.Property("paid", th.BooleanType),
        th.Property("adminUrl", th.StringType),
        th.Property("onlinePaymentLink", th.StringType),
        th.Property("language", th.StringType),
        th.Property("referer", th.StringType),
        th.Property(
            "billingMethod",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("id", th.IntegerType),
            ),
        ),
        th.Property(
            "billingAddress",
            th.ObjectType(
                th.Property("company", th.StringType),
                th.Property("fullName", th.StringType),
                th.Property("street", th.StringType),
                th.Property("houseNumber", th.StringType),
                th.Property("city", th.StringType),
                th.Property("district", th.StringType),
                th.Property("additional", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("countryCode", th.StringType),
                th.Property("regionName", th.StringType),
                th.Property("regionShortcut", th.StringType),
            ),
        ),
        th.Property(
            "deliveryAddress",
            th.ObjectType(
                th.Property("company", th.StringType),
                th.Property("fullName", th.StringType),
                th.Property("street", th.StringType),
                th.Property("houseNumber", th.StringType),
                th.Property("city", th.StringType),
                th.Property("district", th.StringType),
                th.Property("additional", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("countryCode", th.StringType),
                th.Property("regionName", th.StringType),
                th.Property("regionShortcut", th.StringType),
            ),
        ),
        th.Property(
            "status",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "price",
            th.ObjectType(
                th.Property("vat", th.StringType),
                th.Property("toPay", th.StringType),
                th.Property("currencyCode", th.StringType),
                th.Property("withVat", th.StringType),
                th.Property("withoutVat", th.StringType),
                th.Property("exchangeRate", th.StringType),
            ),
        ),
        th.Property(
            "paymentMethod",
            th.ObjectType(
                th.Property("guid", th.StringType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "shipping",
            th.ObjectType(
                th.Property("guid", th.StringType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property("clientIPAddress", th.StringType),
        th.Property(
            "paymentMethods",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "paymentMethod",
                        th.ObjectType(
                            th.Property("guid", th.StringType),
                            th.Property("name", th.StringType),
                        ),
                    ),
                    th.Property("itemId", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "shippings",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "shipping",
                        th.ObjectType(
                            th.Property("guid", th.StringType),
                            th.Property("name", th.StringType),
                        ),
                    ),
                    th.Property("itemId", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "items",
            th.ArrayType(
                th.ObjectType(
                    th.Property("productGuid", th.StringType),
                    th.Property("code", th.StringType),
                    th.Property("itemType", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("variantName", th.StringType),
                    th.Property("brand", th.StringType),
                    th.Property("supplierName", th.StringType),
                    th.Property("remark", th.StringType),
                    th.Property("weight", th.StringType),
                    th.Property("additionalField", th.StringType),
                    th.Property("amount", th.StringType),
                    th.Property("amountUnit", th.StringType),
                    th.Property("amountCompleted", th.StringType),
                    th.Property("priceRatio", th.StringType),
                    th.Property(
                        "itemPrice",
                        th.ObjectType(
                            th.Property("withVat", th.StringType),
                            th.Property("withoutVat", th.StringType),
                            th.Property("vat", th.StringType),
                            th.Property("vatRate", th.StringType),
                        ),
                    ),
                    th.Property(
                        "buyPrice",
                        th.ObjectType(
                            th.Property("withVat", th.StringType),
                            th.Property("withoutVat", th.StringType),
                            th.Property("vat", th.StringType),
                            th.Property("vatRate", th.StringType),
                        ),
                    ),
                    th.Property(
                        "recyclingFee",
                        th.ObjectType(
                            th.Property("category", th.StringType),
                            th.Property("fee", th.StringType),
                        ),
                    ),
                    th.Property(
                        "status",
                        th.ObjectType(
                            th.Property("id", th.IntegerType),
                            th.Property("name", th.StringType),
                        ),
                    ),
                    th.Property(
                        "displayPrices",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("withVat", th.StringType),
                                th.Property("withoutVat", th.StringType),
                                th.Property("vat", th.StringType),
                                th.Property("vatRate", th.StringType),
                            )
                        ),
                    ),
                    th.Property(
                        "mainImage",
                        th.ObjectType(
                            th.Property("name", th.StringType),
                            th.Property("seoName", th.StringType),
                            th.Property("cdnName", th.StringType),
                            th.Property("priority", th.IntegerType),
                            th.Property("description", th.StringType),
                            th.Property("changeTime", th.DateTimeType),
                        ),
                    ),
                    th.Property("stockLocation", th.StringType),
                    th.Property(
                        "surchargeParameters",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property(
                                    "parameterName",
                                    th.Property("code", th.StringType),
                                    th.Property("name", th.StringType),
                                ),
                                th.Property(
                                    "parameterValue",
                                    th.Property("valueIndex", th.StringType),
                                    th.Property("description", th.StringType),
                                    th.Property("price", th.StringType),
                                ),
                            )
                        ),
                    ),
                    th.Property("itemId", th.IntegerType),
                    th.Property("warrantyDescription", th.StringType),
                )
            ),
        ),
        th.Property(
            "notes",
            th.ObjectType(
                th.Property("customerRemark", th.StringType),
                th.Property("eshopRemark", th.StringType),
                th.Property("trackingNumber", th.StringType),
                th.Property("trackingUrl", th.StringType),
                th.Property(
                    "additionalFields",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("index", th.StringType),
                            th.Property("label", th.StringType),
                            th.Property("text", th.StringType),
                        )
                    ),
                ),
            ),
        ),
        th.Property(
            "shippingDetails",
            th.ObjectType(
                th.Property("branchId", th.StringType),
                th.Property("name", th.StringType),
                th.Property("note", th.StringType),
                th.Property("place", th.StringType),
                th.Property("street", th.StringType),
                th.Property("city", th.StringType),
                th.Property("zipCode", th.StringType),
                th.Property("countryCode", th.StringType),
                th.Property("link", th.StringType),
                th.Property("latitude", th.StringType),
                th.Property("longitude", th.StringType),
            ),
        ),
    ).to_dict()

class StockMovementsStream(shoptetStream):
    """Define Stocks List Stream"""

    name = "stocks_movements"
    path = "stocks/{stock_id}/movements"
    primary_keys = ["id"]
    records_jsonpath = "$.data.movements[*]"
    state_partitioning_keys = ["lastId"]
    parent_stream_type = StocksListStream
    replication_key = "changeTime"
    replication_filter = "changeTimeFrom"

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("productCode", th.StringType),
        th.Property("actualAmount", th.StringType),
        th.Property("amountChange", th.StringType),
        th.Property("changedBy", th.StringType),
        th.Property("changeTime", th.DateTimeType),
        th.Property("stock_id", th.IntegerType),
    ).to_dict()
