"""shoptet tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_shoptet.streams import (
    CurrenciesStream,
    OrdersStream,
    PriceListStream,
    PricesStream,
    ProductsStream,
    StocksListStream,
    StocksSuppliesStream,
    StockMovementsStream
)

STREAM_TYPES = [
    OrdersStream,
    ProductsStream,
    StocksListStream,
    StocksSuppliesStream,
    PriceListStream,
    PricesStream,
    CurrenciesStream,
    StockMovementsStream
]


class Tapshoptet(Tap):
    """shoptet tap class."""

    name = "tap-shoptet"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        super().__init__(config, catalog, state, parse_env_config, validate_config)
        self.config_file = config[0]

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "shop",
            th.StringType,
            required=True,
        ),
        th.Property(
            "lookup_days",
            th.IntegerType,
            required=False,
            default=30,
            description="Number of days to look back for changes in data",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property("access_token", th.StringType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapshoptet.cli()
